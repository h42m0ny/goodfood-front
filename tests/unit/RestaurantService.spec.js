import { mount } from "@vue/test-utils";
import RestaurantService from "../../src/services/RestaurantService";

describe("RestaurantService", () => {
    beforeEach(() => {
        fetch.resetMocks();
    })
    test("Return restaurants list", async () => {
        fetch.mockResponseOnce(JSON.stringify({
            data: [
                {
                  id: "60187bda0df50d156fee304b",
                  name: "La marmite",
                  category: "Traditionnelle",
                  geoCoord: "0.111\u00a0;1.09",
                  logo: "/img/logo",
                  description: "Super restaurant traditionnel",
                  phoneNumber: "0202030405",
                  meals: [
                    { name: "Boeuf bourguignon", typeFood: "Fran\u00e7aise" },
                    { name: "Choucroute", typeFood: "Alsacienne" },
                  ],
                  email: "accueil@lamarmite.loc",
                  capacity: 50,
                  address: {
                    line1: "3 rue des anges",
                    line2: null,
                    city: "Brest",
                    zipcode: "29200",
                    country: "France",
                  }
                },
                {
                  id: "6019c2efd2f209f82f64bbe7",
                  name: "Le chaudron",
                  category: "Traditionnelle",
                  geoCoord: "2\u00a0.00089;1.981112",
                  logo: "/img/logo",
                  description: "Restaurant sp\u00e9cialis\u00e9 dans la cuisine traditionnelle",
                  phoneNumber: "0302030405",
                  meals: [
                    {
                      name: "Tripes \u00e0 la mode de caen",
                      typeFood: "Fran\u00e7aise",
                    },
                    { name: "Quiche lorraine", typeFood: "Lorraine" },
                  ],
                  email: "reservation@le-chaudron.loc",
                  capacity: 55,
                  address: {
                    line1: "3 rue des p\u00e2querettes",
                    line2: null,
                    city: "Nancy",
                    zipcode: "54000",
                    country: "France",
                  }
                }
            ]
        }))
        
        const datas = await RestaurantService.getRestaurants();
        expect(datas.data[0].name).toEqual('La marmite')
        
    })
});
