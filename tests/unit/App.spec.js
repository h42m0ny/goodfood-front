import { mount } from '@vue/test-utils';
import App from '../../src/App.vue';

describe('App', () => {
    test('has components', () => {
      expect(typeof App.components).toBe('object')
    }), 
    test('has name defined', () => {
      expect(App.name).toBeDefined()
    }),
    test('has name App', () => {
      expect(App.name).toBe('App')
    })
  });