import { createLocalVue, mount } from "@vue/test-utils";
import Vuetify from 'vuetify'
import Restaurants from "../../src/components/Restaurants.vue";


describe("Restaurants", () => {
  const localVue = createLocalVue();
  let vuetify;
  beforeEach(() => {
    vuetify = new Vuetify()
  });
  test("has data function", () => {
    expect(typeof Restaurants.data).toBe("function");
  }), test("has name defined", () => {
    expect(Restaurants.name).toBeDefined();
  }), test("has name Restaurants", () => {
    expect(Restaurants.name).toBe("Restaurants");
  }),
  test("Display message passing in props", () =>{
    const wrapper = mount(Restaurants,{
      localVue,
      vuetify,
      propsData: {
        msg: "Liste des restaurants"
      }
    })
    expect(wrapper.find("h1").text()).toBe("Liste des restaurants")
        
  })
});
