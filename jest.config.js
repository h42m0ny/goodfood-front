module.exports = {
  automock: false,
  preset: '@vue/cli-plugin-unit-jest',
  setupFiles: ["./setupJest.js"]
}
